class phpcli::drushcron {
  cron { mhfa_drupal_cron:
    require => User['algee'],
    command => '/usr/bin/drush @mhfa.prod cron',
    user => 'algee',
    hour => [23, 7, 17],
    minute => 42,
  }

  cron { mhfa_accreditation_cron:
    require => User['algee'],
    command => "/usr/bin/drush @mhfa.prod ev 'mhfa_accreditation_cron();'",
    user => 'algee',
    hour => 0,
    minute => 12,
  }

  cron { mhfa_drupal_cron_midnight:
    require => User['algee'],
    command => '/usr/bin/drush @mhfa.prod cron',
    user => 'algee',
    hour => 0,
    minute => 42,
  }

  cron { mhfa_drupal_cron_morning:
    require => User['algee'],
    command => '/usr/bin/drush @mhfa.prod cron',
    user => 'algee',
    hour => 8,
    minute => 2,
  }

  cron { mhfa_drupal_cron_night:
    require => User['algee'],
    command => '/usr/bin/drush @mhfa.prod cron',
    user => 'algee',
    hour => 18,
    minute => 12,
  }

  cron { mhfa_drupal_cron_hourly:
    require => User['algee'],
    command => '/usr/bin/drush @mhfa.prod cc drush; /usr/bin/drush @mhfa.prod mhfa-cron',
    user => 'algee',
    hour => absent,
    minute => 24,
  }
}
