class phpcli {
  package { [ 'php-cli',
              'curl', ]:
    ensure => latest,
  }

  package { 'composer':
    ensure => present,
  }

  file { '/usr/local/bin/drush':
    source => 'puppet:///modules/phpcli/drush.phar',
    owner =>  'root',
    group =>  'root',
    mode =>   'a=rx',
  }
}
