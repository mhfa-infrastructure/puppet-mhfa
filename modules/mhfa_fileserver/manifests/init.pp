class mhfa_fileserver {
  package { 'nfs-utils':
    ensure => latest,
  }

  service { [ 'rpcbind',
              'nfs-server',
              'nfs-lock',
              'nfs-idmap', ]:
    ensure => running,
    enable => true,
    require => Package['nfs-utils'],
  }

  file { [ '/var/mhfadata/',
           '/var/mhfadata/drupal_codebase',
           '/var/mhfadata/drupal_files',
           '/var/mhfadata/tmp', ]:
    ensure => "directory",
    owner => 'algee',
    group => 'apache',
    mode => '2775',
    # mode => 'u=rwx,g=rws,o=',
    require => [
      Package['nfs-utils'],
      User['algee'],
    ],
  }

  file { '/etc/exports':
    source => 'puppet:///modules/mhfa_fileserver/exports',
    owner => 'root',
    group => 'root',
    mode => 'u=r,go=',
    notify => Service[ 'rpcbind', 'nfs-server', 'nfs-lock', 'nfs-idmap' ],
  }
}
