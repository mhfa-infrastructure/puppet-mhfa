class mhfa_base::apache {
  package { 'httpd':
    ensure => latest,
  }
}
