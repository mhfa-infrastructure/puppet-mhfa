class mhfa_base::user {
  # mhfa general administrative acccount.
  user { 'mhfa':
    ensure => present,
    password => '*',
    gid => 'mhfa',
    groups => [ 'wheel', 'apache'],
    home => '/home/mhfa',
    managehome => true,
    purge_ssh_keys => true,
    require => [
      Group['mhfa'],
      Package['httpd'],
    ],
  }

  group { 'mhfa':
    ensure => present,
  }

  ssh_authorized_key { 'sunny@mfha-notebook':
    user => 'mhfa',
    type => 'ssh-rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQC6ELbjJadDiUBJPs8rM7rjfD9mS/wJX/YvCh5YGpiOVOierLymodBH9E6Vxr9CK027tzTFg33SAc7B5iWsx3hlY/U2SJayMuEcg794t7II3M3SZUrsX7A5tIxRJXGtDn7MuPqlFBS/PDFFGEnT0qkGsW6WfqDGN1MIgt5+zjxvk+mS+VPg+NJnu4hWCEB/xw0qS2mzNPgHrAMHs02gBVJl0vb1hX5Cu0gf/++LEz15MBe8zYmHxUCroKkT/DPJP6KhyveoBn/87rW2buvdC84EmDPTwgjv26u7EO8huDmD4cdmORrhnH/n6Jcr5DKXBkWZbvKUfKrqZcCrCCf9q45kG6fPccOyXORvq+1UA75LE7e9jnsX5tnoB8B7sC1uVGJ+wqb0tSghTRO0xmWft4I92rz0Bo/UeVUaR7NaPGGoaLA0N3G8gH4S8FLhP9CGMK8JQP7yL1XWR7uzNLmMPFRwKyAs1LL4+nkNe9BvGot8vnRP/lU4TZH17gxD3nHU6qqKsFFtM4qJq2EjtOmUWSJ8ofeSRciqh5dGe/EysYWWmSfpWTpvEDB6t1LVGsSmsFH5c4oOYajWHLpVC4He1lWCberOY/tUiGFKWLHNyQnmmYZ1M+ITnMnYQ9F8cWPCAUL8CLQR3u8QTQ8ADqMzCxgxnvg5BDa0FbFqhLkNByc8iQ==',
  }

  ssh_authorized_key { 'sunny@mfha-notebook2':
    user => 'mhfa',
    type => 'ssh-rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQDJT3uBrFViCnnl/a9YICjpRB0uxDb2ONgRxkRx9NhCL1axaQTG0VMxWdzOSklUNU2PXmGsxkEFIa7kYr4G7PS1TtD8u2oB4LRAaMIxYBK+wmZ8iISIgcezjO9njDV860jSmG7PnG8FyG2Mpmswk8XUG66Z8+sh91tpSzgJ9e0wPg30fM19uiVTseGuieKSKhP9EH5KHcjAKf0ou8L1HIALB/QWANWTGTwehZHZ7zFMeCu1Q/4jj3HsLJiYuzPUS17bq9YsS+Ak+AAWfd77Kmmf67LC5eybGJIghFNJ1ndzVLFVJzejl3AEBJxds/2xffSoolKXcBLkmr0cgxG6ZckQzN6702ufdnXNmTqWcahqiP6DyZRsN/hXxuMBm5YLb1snZYgaBLjfT13sGUTFxzYIZ72jsxV+oZ7YfGK4CgpTxeo2Jvwi+4WVH8z0lcikOnHQQT8GPlrYyO7+XGE7xRMB7txJSoZJG5pEVmr26vKzw3KbGK9zCEpaqrWukMhawnrACYRzZ7oXZvv2urK40oVvXoYjV8H4K9TXGLWyW76/I0vfjFf1SjMGNod504TcEVqLuvYBYKqR1csKKTRGUU91myjAunfnZT9UhzxcXR+dpFd4U+wVgBfZxHq33weqJ/GneWEv4O7QueDK4bPYsTD7MhiBx3uBR2fZq9PtmY+qxQ==',
  }

  ssh_authorized_key { 'sunny@mfha-phone':
    user => 'mhfa',
    ensure => absent,
  }

  ssh_authorized_key { 'brendanokeefe@RA2s-iMac.local':
    user => 'mhfa',
    type => 'ssh-rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQDHBiVz8aVR6QhrWAo20CyQ+rEXA5HTmH2d63oX/3wIqa5/enqR5xBikoJVhl2mfrXtleCk6vVzXWa8eCm+Pr6zrheWH4iz9LYL5qPsOuWwZeiOSVX50IAstbgOcRoIboQ9+yJ2ytUvVD9PBsX6QVv+45Oq874V6zSQ6coVvMYH0cdiCiOxNZooI3WD0e019++lXY9kDnhRY5kAf7+ENlYBfsJGmLqJtmzKK2U2aYviRgoZVamZavWzZ88zDDTI2/uQoCDvzv+EjSTDfZ+AdXJ7QhHZjE5UwQsDSNEqNabw9WUZZHwwrf+DnvhbvzVnWl5ISAe5N5IEPNIlkUDhnCR0K1bt/A+Rvku+Z6h0JHt0IDdXsw9mYf388RjCy/Xs+NkqapLdnaVLrD953cy2PlE3snqkMVQCyvuYeQ3vTOAww9+ZT4YjVghBZNFm2wI4afYVKQRoIxM4mvHg6uHj32yP5yBUQlawn/QN2AoMw6uBBLibQPSvcksmfghnIM7D7dZIDJwuUzJHFNyhqczTmVLtgSMVaHbZ2rhYQHF7eBA6wIVQ8OFPGc8O7XdppcByTrmtTk38fioFEf4jwxITT1SsTXLcVmsxVeJwT3GnPEUQi3EZyCjuHQj9BvRvb14Ki/lufZL9TYYaWNv5FnX9v7QjEROPZ9FLXseFhCdorVgs8w==',
  }

  ssh_authorized_key { 'rene@Renes-MacBook-Pro.local':
    user => 'mhfa',
    type => 'ssh-rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQDG5rHcHLsyarVc6qis2Y5DM/CWlz9REHyarpC6zEAoStIspkrIG/FqCLB/HuOlPgOK1OLjV5b9hrSr4AwBysVnMD+KoBhmmRw081XHvPub/6tX8Kc0Eo6a2OITMnNeU7srGopLh+jd1V+1RDZrNaSJTtS7/khPflvDlupC9LOq8dE4aOOT+Gni9NweHc4tSsonSl+F75rYdz2cetupqO2LgBHuwuf7aYfFI1veESfi6vy3qq6hMSt0vNd9i7qrLrJtbY9A9g9eo/WW0JghkkolrHvYNrlfo8jMCZ8n88U+JN9f6Wh0ruuBfp38ZQxmgyAP+PSV17eAcyjItdGZo0b4Z06bP+o4K86C5VxkLQWT7CRK8Y8VGb8Nxhi1oUoDa1KgtxIHz9BpPu4AuO283O8KBs9qmrDo/sP3rW0bQkAcMRPKXRdrFAEIXFHwDYj3qOe5MQxBvNfjB8nLQYVX1hIMmfAoCLUv803KI4X+fOZQz1l7zWUFGabRqcmkNzTHP1VcOCvdC1Ae1goOohHK6hwFGCZSw6rpe0W6+cBoDQnroHAg1yPanAx8K3PbjBBydY45loOvaam5WNr2sFK3053R2J3l4FvaKXXAqwdZE35FFxcImyYBDwYOcHg8cWf9ceEzLTaVx3oT2FWQRYzIwR/LHEONFvzGCxLj4iShfjGTlw==',
  }

  ssh_authorized_key { 'mariat@mhfa.com.au':
    user => 'mhfa',
    type => 'ssh-rsa',
    key  => chomp(file('mhfa_base/ssh/mariat.pub.txt')),
  }

  ssh_authorized_key { 'graemew@Moon-Rabbit':
    user => 'mhfa',
    type => 'ssh-rsa',
    key  => chomp(file('mhfa_base/ssh/graemew.pub.txt')),
  }

  ssh_authorized_key { 'stuart@deciphered.ld':
    user => 'mhfa',
    type => 'ssh-rsa',
    key  => chomp(file('mhfa_base/ssh/stuart.pub.txt')),
  }

  ssh_authorized_key { 'brian@zesty.tech':
    user => 'mhfa',
    type => 'ssh-rsa',
    key  => chomp(file('mhfa_base/ssh/brian.pub.txt')),
  }

  ssh_authorized_key { 'sunnokia6':
      user => 'mhfa',
      type => 'ssh-rsa',
      key  => chomp(file('mhfa_base/ssh/sunnokia6.pub.txt')),
  }

  ssh_authorized_key { 'melissa':
      user => 'mhfa',
      type => 'ssh-rsa',
      key  => chomp(file('mhfa_base/ssh/melissa.pub.txt')),
  }

  ssh_authorized_key { 'robbie':
      user => 'mhfa',
      type => 'ssh-rsa',
      key  => chomp(file('mhfa_base/ssh/robbie.pub.txt')),
  }

  # algee:bear are mhfa's deployment user account, used for deploying
  # code only.
  user { 'algee':
    ensure => present,
    password => '*',
    uid => 2015,
    gid => 'bear',
    groups => 'apache',
    home => '/home/algee',
    managehome => true,
    purge_ssh_keys => true,
    require => [
      Group['bear'],
      Package['httpd'],
    ],
  }

  group { 'bear':
    ensure => present,
    gid => 2015,
  }

  file { '/etc/sudoers.d/mhfa-sudoers':
    source => 'puppet:///modules/mhfa_base/mhfa-sudoers',
    owner => 'root',
    group => 'root',
    mode => 'ug=r,o=',
  }
}
