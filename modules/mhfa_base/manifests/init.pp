class mhfa_base (
  $resolv = true,
) {
  package { 'epel-release':
    ensure => installed,
  }

  package { [ 'openssh-server', 'deltarpm', 'tree', 'tmux' ]:
    ensure => latest,
  }

  service { 'sshd':
    ensure => running,
    require => Package['openssh-server'],
  }

  file { '/etc/ssh/sshd_config':
    source => 'puppet:///modules/mhfa_base/sshd_config',
    notify => Service['sshd'],
    owner => 'root',
    group => 'root',
    mode => 'u=r,go=',
  }

  if $resolv {
    file { '/etc/resolv.conf':
      source => 'puppet:///modules/mhfa_base/resolv.conf',
      owner => 'root',
      group => 'root',
      mode => 'a=r',
    }

    file { '/etc/rc.d/rc.local':
      source => 'puppet:///modules/mhfa_base/rc.local',
      owner => 'root',
      group => 'root',
      mode => '0555',
    }
  }

  file { '/etc/resolv.mhfa.conf':
    source => 'puppet:///modules/mhfa_base/resolv.conf',
    owner => 'root',
    group => 'root',
    mode => 'a=r',
  }

  file { '/etc/issue.net':
    source => 'puppet:///modules/mhfa_base/issue.net',
    owner => 'root',
    group => 'root',
    mode => 'a=r',
  }

  file { '/etc/gitconfig':
    source => 'puppet:///modules/mhfa_base/gitconfig',
    owner => 'root',
    group => 'root',
    mode => 'a=r',
  }

  file { '/etc/localtime':
    ensure => link,
    target => '/usr/share/zoneinfo/Australia/Melbourne',
  }
}
