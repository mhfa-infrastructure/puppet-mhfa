class mhfa_base::prod {
  file { '/etc/profile.d/production.sh':
    source => 'puppet:///modules/mhfa_base/production.sh',
    owner => root,
    group => root,
    mode => 'a=r',
  }
}
