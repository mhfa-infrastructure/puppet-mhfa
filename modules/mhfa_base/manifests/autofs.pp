class mhfa_base::autofs {
  file { '/usr/local/bin/httpd-start':
    source => 'puppet:///modules/mhfa_base/httpd-start',
    owner => root,
    group => root,
    mode => 'u=rx,go=',
  }

  cron { httpd-start:
    require => File['/usr/local/bin/httpd-start'],
    command => '/usr/local/bin/httpd-start',
    special => 'reboot',
    user => 'root',
  }
}
