class mhfa_base::apache::service (
  $service_enable = true,
  $service_ensure = running,
) {
  include mhfa_base::apache

  package { 'mod_ssl':
    ensure => latest,
  }

  service { 'httpd':
    require => Package['httpd'],
    ensure => $service_ensure,
    enable => $service_enable,
  }

  File {
    owner => 'root',
    group => 'root',
    mode => 'ugo=r',
  }

  file { '/etc/httpd/conf.d/obscurity.conf':
    require => Package['httpd'],
    source => 'puppet:///modules/mhfa_base/obscurity.conf',
    notify => Service['httpd'],
  }

  file { '/etc/httpd/conf.modules.d/00-mpm.conf':
    require => Package['httpd'],
    source => 'puppet:///modules/mhfa_base/event-mpm.conf',
    notify => Service['httpd'],
  }
}
