# == class: memcached
#
# installs and runs memcached service, accessible by localhost only by
# default.
#
# === parameters
#
# [*options*]
# -l specifies interface to listen on (default: "-l 127.0.0.1")
# You may specify multiple addresses separated by comma or by using -l
# multiple times, for example:
# -l 127.0.0.1 -l 192.168.1.1
#
# [*port*]
# tcp port number to listen on (default: 11211).
#
# [*user*]
# user account memcache will be running as.
#
# [*maxconn*]
# max simultaneous connections (default: 1024).
#
# [*cachesize*]
# max memory to use for items in megabytes (default: 1024 MB).
#
# === examples
#
# class { 'memcached':
#   options => "-l ${ipaddress_lo}  -l ${ipaddress_eth1}",
# }
#
class memcached (
  $options = '"-l 127.0.0.1"',
  $port = 11211,
  $user = 'memcached',
  $maxconn = 1024,
  $cachesize = 1024,
) {
  package { 'memcached':
    ensure => installed,
  }

  service { 'memcached':
    ensure  => running,
    enable  => true,
    require => Package['memcached'],
  }

  file { '/etc/sysconfig/memcached':
    content => template('memcached/sysconfig/memcached.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => 'u=r,go=',
    notify  => Service['memcached'],
  }
}
