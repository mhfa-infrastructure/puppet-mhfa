class supervisor {
  package { 'supervisor':
    ensure => present,
  }

  service { 'supervisord':
    ensure => running,
    enable => true,
    require => Package['supervisor'],
  }
}
