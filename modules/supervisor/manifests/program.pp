define supervisor::program (
  $command,
  $autostart = 'true',
  $autorestart = 'true',
  $startretries = 3,
  $stderr_logfile = '/var/log/messages',
  $stdout_logfile = '/var/log/messages',
  $program = $title,
) {
  include supervisor

  file { "/etc/supervisord.d/${program}.ini":
    content => template('supervisor/program.ini.erb'),
    owner => 'root',
    group => 'root',
    mode => 'a=r',
    notify => Service['supervisord'],
  }
}
