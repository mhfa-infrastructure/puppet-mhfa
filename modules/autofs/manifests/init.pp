class autofs {
  package { [ 'autofs', 'nfs-utils' ]:
    ensure => latest,
  }

  service { 'autofs':
    require => Package['autofs'],
    ensure => running,
    enable => true,
  }

  File {
    owner => 'root',
    group => 'root',
    mode => 'a=r',
  }

  file { '/etc/auto.master':
    source => 'puppet:///modules/autofs/auto.master',
    notify => Service['autofs'],
  }

  file { '/etc/auto.misc':
    require => File['/etc/auto.master'],
    source => 'puppet:///modules/autofs/auto.misc',
    notify => Service['autofs'],
  }

  file { '/var/mhfadata':
    ensure => link,
    target => '/misc/mhfadata',
  }
}
