class rackspace_monitoring_agent {
  yumrepo { 'rackspace-cloud-monitoring':
    target => '/etc/yum.repos.d/rackspace-cloud-monitoring.repo',
    ensure => present,
    descr => 'Rackspace Monitoring',
    baseurl => 'http://stable.packages.cloudmonitoring.rackspace.com/redhat-7-x86_64',
    gpgkey => 'https://monitoring.api.rackspacecloud.com/pki/agent/redhat-7.asc',
    enabled => 1,
  }

  package { 'rackspace-monitoring-agent':
    require => Yumrepo['rackspace-cloud-monitoring'],
    ensure => present,
  }

  service { 'rackspace-monitoring-agent':
    require => Package['rackspace-monitoring-agent'],
    ensure => running,
    enable => true,
  }
}
