class mariadb {
  package { 'mariadb-server':
    ensure => latest,
  }

  service { 'mariadb':
    ensure => running,
    enable => true,
    require => Package['mariadb-server'],
  }

  file { '/etc/my.cnf':
    source => [
      "puppet:///modules/mariadb/$fqdn.my.cnf",
      "puppet:///modules/mariadb/$hostname.my.cnf",
      'puppet:///modules/mariadb/my.cnf',
    ],
    notify => Service['mariadb'],
    owner => 'root',
    group => 'root',
    mode => 'ugo=r',
  }
}
