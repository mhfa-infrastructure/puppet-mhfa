class mailtrap {
  package { 'postfix':
    require => Package['sendmail'],
    ensure => latest,
  }

  # SASL authentication libraries for logging on to SMTP servers.
  package { 'cyrus-sasl-plain':
    ensure => latest,
  }

  package { 'sendmail':
    ensure => absent,
  }

  service { 'postfix':
    require => Package['postfix'],
    ensure => running,
    enable => true,
  }

  File {
    owner => 'root',
    group => 'root',
    mode => 'a=r',
  }

  file { '/etc/postfix/virtual':
    source => [
      "puppet:///modules/mailtrap/${fqdn}.virtual",
      "puppet:///modules/mailtrap/${hostname}.virtual",
      'puppet:///modules/mailtrap/virtual',
    ],
    notify => Service['postfix'],
  }

  file { '/etc/postfix/main.cf':
    require => File['/etc/postfix/virtual'],
    source => [
      "puppet:///modules/mailtrap/${fqdn}.main.cf",
      "puppet:///modules/mailtrap/${hostname}.main.cf",
      'puppet:///modules/mailtrap/main.cf',
    ],
    notify => Service['postfix'],
  }

  file { '/etc/postfix/sender_relay':
    source => 'puppet:///modules/mailtrap/sender_relay',
    notify => Service['postfix'],
  }

  file { '/etc/postfix/header_checks':
    source => 'puppet:///modules/mailtrap/header_checks',
    notify => Service['postfix'],
  }
}
