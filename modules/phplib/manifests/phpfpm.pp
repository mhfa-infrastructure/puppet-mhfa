class phplib::phpfpm {
  package { 'php-fpm':
    ensure => latest,
  }

  service { 'php-fpm':
    require => Package['php-fpm'],
    ensure => running,
    enable => true,
    subscribe => File['/etc/php.d/mhfa.ini'],
  }

  file { '/etc/php-fpm.d/mhfa-phpfpm-ini.conf':
    require => Package['php-fpm'],
    source => 'puppet:///modules/phplib/mhfa-phpfpm-ini.conf',
    owner => 'root',
    group => 'root',
    mode => 'u=r,go=',
    notify => Service['php-fpm'],
  }
}
