class phplib {
  require wkhtmltopdf

  $phplib_packages = [
    'php-devel',
    'php-gd',
    'php-ldap',
    'php-mbstring',
    'php-mysqlnd',
    'php-pdo',
    'php-pear',
    'php-pecl-memcache',
    'php-pecl-xdebug',
    'php-pecl-zendopcache',
    'php-xml',
  ]

  package { $phplib_packages:
    ensure => latest,
  }

  File {
    owner => 'root',
    group => 'root',
    mode => 'a=r',
  }

  file { '/etc/php.d/mhfa.ini':
    source => 'puppet:///modules/phplib/mhfa.ini',
  }

  file { '/etc/php.d/xdebug.ini':
    ensure => absent,
    source => 'puppet:///modules/phplib/xdebug.ini',
  }

  exec { 'yum_group_dev_tools':
    unless => '/usr/bin/yum group list "Development Tools" | /usr/bin/grep -q Installed',
    command => '/usr/bin/yum -y group install "Development Tools"',
    user => 'root',
  }

  # PECL uploadprogress library for Drupal.
  exec { 'pecl_uploadprogress':
    require => [
      Package['php-devel', 'php-pear'],
      Exec['yum_group_dev_tools'],
    ],
    unless => '/usr/bin/pecl list | /usr/bin/grep -q uploadprogress',
    command => '/usr/bin/pecl channel-update pecl.php.net && /usr/bin/pecl install uploadprogress',
    user => 'root',
  }
}
