# Class: wkhtmltopdf
#
# Installs wkhtmltopdf 12.3 and required packages and scripts to be used by web apps.
#
# The current version is distributed as a generic linux binary. 
# This is being stored in the files folder for this module. Possibly not the best practice,
# may be better to create a package from source for the applicable env.
#
class wkhtmltopdf {
  # Install required packages
  $wkhtmltopdf_req_packages = [
    'zlib',
    'urw-fonts',
    'fontconfig',
    'freetype',
    'libX11',
    'libXext',
    'libXrender',
  ]

  package { $wkhtmltopdf_req_packages:
    ensure => installed,
  }
  # Install wkhtmltopdf binary
  file { '/usr/local/bin/wkhtmltopdf':
    require => Package[$wkhtmltopdf_req_packages],
    source  => 'puppet:///modules/wkhtmltopdf/wkhtmltopdf',
    mode    => 'a=rx',
  }

}
