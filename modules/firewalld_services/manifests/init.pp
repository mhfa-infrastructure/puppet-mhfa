class firewalld_services {
  File {
    owner => 'root',
    group => 'root',
    mode => 'u=r,go=',
  }

  file { '/etc/firewalld/services/mountd.xml':
    source => 'puppet:///modules/firewalld_services/mountd.xml',
  }

  file { '/etc/firewalld/services/rpc-bind.xml':
    source => 'puppet:///modules/firewalld_services/rpc-bind.xml',
  }

  file { '/etc/firewalld/services/mhfa.xml':
    source => [
      "puppet:///modules/firewalld_services/${fqdn}.mhfa.xml",
      "puppet:///modules/firewalld_services/${hostname}.mhfa.xml",
      'puppet:///modules/firewalld_services/mhfa.xml',
    ],
  }

  # to reduce noise on ssh auth log we run ssh on port 1. we still
  # run ssh on port 22, but only for the first hour after reboot,
  # just in case we lose access to the machine on port 1 after a
  # reboot.
  cron { tmp_ssh_port:
    command => '/usr/bin/firewall-cmd --add-service=ssh --timeout=3600',
    user => 'root',
    special => 'reboot',
  }
}
