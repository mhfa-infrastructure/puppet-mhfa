#!/usr/bin/env bash

set -e

DATE=$(/usr/bin/python -c "from datetime import datetime;print(datetime.now().strftime('0%u-%a-%p').lower())")
SNAP_PATH="/var/mhfadata/dbsnapshots/mhfa-prod-${DATE}.sql.gz"
SNAP_PATH_MINI="/var/mhfadata/dbsnapshots/mhfa-prod-mini-${DATE}.sql.gz"
EXCLUDE_TABLES='search_index,cache,cache_*,*_cache'
EXCLUDE_TABLES_MINI="${EXCLUDE_TABLES},*_revision_*,field_data_field_message_*,message"

echo $DATE
echo $SNAP_PATH
echo $SNAP_PATH_MINI
echo $EXCLUDE_TABLES
echo $EXCLUDE_TABLES_MINI

/usr/bin/drush @mhfa.prod sql-dump --structure-tables-list=$EXCLUDE_TABLES | /usr/bin/gzip -9 -n > $SNAP_PATH

/usr/bin/ln -s -f $SNAP_PATH /home/mhfa/db-snapshots/mhfa-prod-current.sql.gz

/usr/bin/drush @mhfa.prod sql-dump --structure-tables-list=$EXCLUDE_TABLES_MINI | /usr/bin/gzip -9 -n > $SNAP_PATH_MINI

/usr/bin/ln -s -f $SNAP_PATH_MINI /home/mhfa/db-snapshots/mhfa-prod-mini.sql.gz

