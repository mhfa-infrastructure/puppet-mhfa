# creates a snapshot of the mhfa-drupal db twice a day.
class dbsnapshots {
  file { '/home/mhfa/db-snapshots':
    ensure => directory,
    owner  => 'mhfa',
    group  => 'mhfa',
    mode   => '0660',
  }

  file { '/var/mhfadata/dbsnapshots':
    ensure => directory,
    owner  => 'mhfa',
    group  => 'mhfa',
    mode   => '0660',
  }

  file { '/usr/local/bin/dbsnapshots.sh':
    ensure => present,
    source => 'puppet:///modules/dbsnapshots/dbsnapshots.sh',
    owner  => 'root',
    group  => 'root',
    mode   => '0555',
  }

  cron { 'dbsnapshots':
    require => File[
      '/home/mhfa/db-snapshots',
      '/usr/local/bin/dbsnapshots.sh',
      '/var/mhfadata/dbsnapshots'
    ],
    command => 'time(/usr/bin/nice /usr/local/bin/dbsnapshots.sh)',
    user    => 'mhfa',
    hour    => [3, 15],
    minute  => 12,
  }
}
