node 'backend-syd01.mhfa.net.au' {
  include mhfa_base
  include mhfa_base::apache
  include mhfa_base::user
  include mhfa_base::prod
  include mhfa_node
  include rackspace_monitoring_agent
  include phplib
  include phpcli
  include phpcli::drushcron
  include mhfa_fileserver
  include mailtrap
  include dbsnapshots
  include firewalld_services

  class { 'memcached':
    options => "\"-l ${ipaddress_eth2}  -l ${ipaddress_lo}\"",
    maxconn => 1280,
    cachesize => 1280,
  }

  package { 'puppet-server':
    ensure => latest,
  }

  service { 'puppetmaster':
    ensure => running,
    enable => true,
    require => Package['puppet-server'],
  }

  package { 'driveclient':
    ensure => latest,
  }

  service { 'driveclient':
    ensure => running,
    enable => true,
    require => Package['driveclient'],
  }
}

node 'drupal-syd01.mhfa.net.au' {
  include mhfa_base
  include mhfa_base::apache::service
  include mhfa_base::user
  include mhfa_base::prod
  include mhfa_base::autofs
  include autofs
  include mhfa_node
  include rackspace_monitoring_agent
  include mailtrap
  include phplib
  include phplib::phpfpm
  include firewalld_services

  class { 'memcached':
    options => "-l ${ipaddress_eth2}  -l ${ipaddress_lo}",
    cachesize => $memorysize_mb / 4,
  }
}

node 'moodle-syd01.mhfa.net.au' {
  include mhfa_base
  include mhfa_base::apache
  include mhfa_base::user
  include mhfa_base::prod
  include mhfa_node
  include mailtrap
  include rackspace_monitoring_agent

  class { 'memcached':
    options => "-l ${ipaddress_eth2}  -l ${ipaddress_lo}",
    cachesize => $memorysize_mb / 4,
  }
}

node 'cache-syd01.mhfa.net.au' {
  include mhfa_base
  include mhfa_base::apache
  include mhfa_base::user
  include mhfa_base::prod
  include mhfa_node
  include mailtrap
  include rackspace_monitoring_agent
  include firewalld_services

  class { 'memcached':
    options => "-l ${ipaddress_eth2}  -l ${ipaddress_lo}",
    cachesize => $memorysize_mb * 0.8,
  }
}

node 'sqldb-syd01.mhfa.net.au' {
  include mhfa_base
  include mhfa_base::apache
  include mhfa_base::user
  include mhfa_base::prod
  include mhfa_node
  include rackspace_monitoring_agent
  include mariadb
  include mailtrap
  include firewalld_services
}

node 'stage-syd01.mhfa.net.au' {
  include mhfa_base
  #include mhfa_base::apache::service
  include mhfa_base::user
  include rackspace_monitoring_agent
  include mariadb
  include phplib
  include phplib::phpfpm
  include phpcli
  include mailtrap
  include firewalld_services
  include docker

  class { 'memcached':
    options => "\"-l ${ipaddress_eth1} -l ${ipaddress_lo}\"",
    maxconn => 1280,
    cachesize => $memorysize_mb / 4,
  }

  class { 'mhfa_base::apache::service':
    service_enable => false,
    service_ensure => stopped,
  }

  supervisor::program { 'httpd':
    command => '/usr/sbin/httpd -DFOREGROUND',
    startretries => 99,
  }

  docker::image { 'fedora':
    image_tag => '22',
  }

  package { 'puppet':
    ensure => present,
  }

  service { 'puppet':
    ensure => stopped,
    enable => false,
    require => Package['puppet'],
  }
}

node default {
  class { 'mhfa_base':
    resolv => false,
  }

  include mhfa_base::user
  include mariadb
  include phplib
  include phplib::phpfpm
  include phpcli
  include mailtrap

  class { 'memcached':
    options => "\"-l ${ipaddress_lo}\"",
    maxconn => 1280,
    cachesize => $memorysize_mb / 4,
  }

  class { 'mhfa_base::apache::service':
    service_enable => false,
    service_ensure => stopped,
  }

  supervisor::program { 'httpd':
    command => '/usr/sbin/httpd -DFOREGROUND',
    startretries => 99,
  }

  package { 'puppet':
    ensure => present,
  }

  service { 'puppet':
    ensure => stopped,
    enable => false,
    require => Package['puppet'],
  }
}
